package fi.eerosalla.plugin.survival.bank.commands;

import com.j256.ormlite.stmt.QueryBuilder;
import fi.eerosalla.plugin.survival.bank.Bank;
import fi.eerosalla.plugin.survival.bank.BankConfig;
import fi.eerosalla.plugin.survival.bank.database.model.BankPlayer;
import fi.eerosalla.plugin.survival.bank.util.ItemUtil;
import fi.eerosalla.plugin.survival.bank.util.MsgUtil;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static fi.eerosalla.plugin.survival.bank.util.SchedulerUtil.async;
import static fi.eerosalla.plugin.survival.bank.util.SchedulerUtil.sync;


public record BankCommand(Bank plugin) implements CommandExecutor {

	private final static Pattern NAME_PATTERN = Pattern.compile("^\\w{3,16}$");
	private final static Pattern INTEGER_PATTERN = Pattern.compile("^\\d{1,12}$");
	private final static Pattern TRANSACTION_TYPE_PATTERN = Pattern.compile("^(give|take|set)$");

	private final static int BALTOP_CACHE_SIZE = 100;

	@Override
	public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, String label, @NotNull String[] args) {
		String errorStr;
		switch (label.toLowerCase()) {
			case "bal":
			case "balance":
			case "ebal":
			case "ebalance":
			case "money":
			case "emoney":
				errorStr = balanceCommand(sender, label, args);
				break;
			case "pay":
			case "epay":
				errorStr = payCommand(sender, label, args);
				break;
			case "baltop":
			case "ebaltop":
				errorStr = baltopCommand(sender, label, args);
				break;
			case "eco":
				errorStr = ecoCommand(sender, label, args);
				break;
			case "bank":
			case "bankgui":
				errorStr = bankCommand(sender, label);
				break;
			default:
				return false;
		}

		if(errorStr != null){
			sender.sendMessage(errorStr);
		}

		return true;
	}


	public String balanceCommand(CommandSender sender, String label, String[] args) {

		if (args.length == 0 && sender instanceof Player player) {

			if (!sender.hasPermission("bank.cmd.balance")) {
				return BankConfig.Lang.CMD_NO_PERMISSION;
			}

			BankPlayer bankPlayer = plugin.playerService.getPlayerFromCache(player.getUniqueId());

			if (bankPlayer == null) {
				return BankConfig.Lang.CMD_INTERNAL_ERROR;
			}

			long balance = bankPlayer.getBalance();

			player.sendMessage(
					String.format(BankConfig.Lang.CMD_BALANCE_SELF_BALANCE, MsgUtil.formatCurrency(balance))
			);

		} else if (args.length == 1 && NAME_PATTERN.matcher(args[0]).matches()) {

			if (!sender.hasPermission("bank.cmd.balance.other")) {
				return BankConfig.Lang.CMD_NO_PERMISSION;
			}

			async(() -> {
				try {
					BankPlayer bankPlayer = plugin.playerService.getPlayer(args[0]).get(5, TimeUnit.SECONDS);

					if (bankPlayer == null) {
						sender.sendMessage(BankConfig.Lang.CMD_PLAYER_NOT_FOUND);

						return;
					}

					sync(() -> {
						long balance = bankPlayer.getBalance();

						sender.sendMessage(
								String.format(BankConfig.Lang.CMD_BALANCE_OTHER_BALANCE, args[0], MsgUtil.formatCurrency(balance))
						);
					});
				} catch (InterruptedException | ExecutionException | TimeoutException ex) {
					sender.sendMessage(BankConfig.Lang.CMD_INTERNAL_ERROR);

					ex.printStackTrace();
				}
			});

		} else {
			return String.format(BankConfig.Lang.CMD_BALANCE_USAGE, label);
		}

		return null;
	}


	public String payCommand(CommandSender sender, String label, String[] args) {

		if (!(sender instanceof Player)) {
			return BankConfig.Lang.CMD_GAME_ONLY;
		}

		if (!sender.hasPermission("bank.cmd.pay")) {
			return BankConfig.Lang.CMD_NO_PERMISSION;
		}

		if (args.length == 2
				&& NAME_PATTERN.matcher(args[0]).matches()
				&& INTEGER_PATTERN.matcher(args[1]).matches()
		) {

			Player receiver = Bukkit.getPlayer(args[0]);
			if (receiver == null) {
				return BankConfig.Lang.CMD_PLAYER_NOT_FOUND;
			}

			Player player = (Player) sender;
			BankPlayer senderData = plugin.playerService.getPlayerFromCache(player.getUniqueId());
			BankPlayer receiverData = plugin.playerService.getPlayerFromCache(receiver.getUniqueId());

			if (senderData == null || receiverData == null) {
				return BankConfig.Lang.CMD_INTERNAL_ERROR;
			}

			long amount = Long.parseLong(args[1]);
			if (amount < 1) {
				return String.format(BankConfig.Lang.CMD_PAY_USAGE, label);
			}

			if (!senderData.withdraw(amount)) {
				return BankConfig.Lang.INSUFFICIENT_FUNDS;
			}

			receiverData.deposit(amount);

			String amountStr = MsgUtil.formatCurrency(amount);
			sender.sendMessage(String.format(BankConfig.Lang.CMD_PAY_SENT, amountStr, receiver.getName()));
			receiver.sendMessage(String.format(BankConfig.Lang.CMD_PAY_RECEIVED, amountStr, player.getName()));
		} else {
			return String.format(BankConfig.Lang.CMD_PAY_USAGE, label);
		}

		return null;
	}

	private final static ArrayList<Map.Entry<String, Long>> baltopCache = new ArrayList<>(BALTOP_CACHE_SIZE);
	private volatile static long baltopCacheLastUpdate = 0;
	public String baltopCommand(CommandSender sender, String label, String[] args){

		if(!sender.hasPermission("bank.cmd.baltop")){
			return BankConfig.Lang.CMD_NO_PERMISSION;
		}

		int page;
		if(args.length > 0){

			if(!INTEGER_PATTERN.matcher(args[0]).matches()){
				return BankConfig.Lang.CMD_BALTOP_USAGE;
			} else {
				page = (int) Long.parseLong(args[0]);

				if(page < 1 || page > 10){
					return String.format(BankConfig.Lang.CMD_BALTOP_INVALID_PAGE, label);
				}
			}

		} else {
			page = 1;
		}

		async(() -> {
			long now = System.currentTimeMillis();

			if(now - baltopCacheLastUpdate >= 1000L * 60L * 2L) {
				plugin.getLogger().info("Updating baltop..");

				try {
					QueryBuilder<BankPlayer, UUID> query = plugin.playerService.query();
					query.orderBy("balance", false);
					query.limit((long) BALTOP_CACHE_SIZE);

					List<BankPlayer> list = query.query();
					if(list != null){
						final List<Map.Entry<String, Long>> baltopEntries = list.stream().map((data) -> Map.entry(data.getName(), data.getBankBalance())).collect(Collectors.toList());

						sync(() -> {
							baltopCache.clear();
							baltopCache.addAll(baltopEntries);

							sendBaltop(sender, page);
						});
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}

				baltopCacheLastUpdate = now;
			} else {

				sync(() -> sendBaltop(sender, page));

			}

		});

		return null;
	}

	private void sendBaltop(CommandSender sender, int page){
		StringBuilder builder = new StringBuilder();

		builder.append(String.format(BankConfig.Lang.CMD_BALTOP_HEADER, page)).append('\n');

		int pageStart = (page - 1) * 10;
		int pageEnd = page * 10;
		int n = (page - 1) * 10 + 1;
		for(int i = pageStart; i < pageEnd; ++i){
			if(i >= baltopCache.size()){
				break;
			}

			Map.Entry<String, Long> entry = baltopCache.get(i);

			builder.append(String.format(BankConfig.Lang.CMD_BALTOP_ENTRY, n, entry.getKey(), MsgUtil.formatCurrency(entry.getValue())));
			builder.append(ChatColor.WHITE);

			if(i != pageEnd - 1 && i != baltopCache.size() - 1){
				builder.append('\n');
			}
			++n;
		}

		sender.sendMessage(builder.toString());
	}

	private String bankCommand(CommandSender sender, String label){

		if (!(sender instanceof Player)) {
			return BankConfig.Lang.CMD_GAME_ONLY;
		}

		if(!sender.hasPermission("bank.cmd.bank")){
			return BankConfig.Lang.CMD_NO_PERMISSION;
		}

		plugin.guiManager.openView((Player) sender, "AccountView");

		return null;
	}

	private String ecoCommand(CommandSender sender, String label, String[] args){

		if(!sender.hasPermission("bank.cmd.eco")){
			return BankConfig.Lang.CMD_NO_PERMISSION;
		}

		if(args.length == 0){
			return BankConfig.Lang.CMD_ECO_FULL_USAGE;
		} else if(args.length == 3
				&& TRANSACTION_TYPE_PATTERN.matcher(args[0]).matches()
				&& NAME_PATTERN.matcher(args[1]).matches()
				&& INTEGER_PATTERN.matcher(args[2]).matches()
		) {

			final String action = args[0];
			final String playerName = args[1];
			final long amount = Long.parseLong(args[2]);

			async(() -> {
				try {
					BankPlayer data = plugin.playerService.getPlayer(playerName).get(5, TimeUnit.SECONDS);
					if(data == null){
						sender.sendMessage(BankConfig.Lang.CMD_PLAYER_NOT_FOUND);

						return;
					}

					sync(() -> {

						switch(action){
							case "give":
								data.deposit(amount);

								sender.sendMessage(String.format(
										BankConfig.Lang.CMD_ECO_GIVE_SUCCESSFUL,
										MsgUtil.formatCurrency(amount),
										data.getDisplayName()
								));
								break;
							case "take":
								if(data.withdraw(amount)){
									sender.sendMessage(
											String.format(
													BankConfig.Lang.CMD_ECO_TAKE_SUCCESSFUL,
													MsgUtil.formatCurrency(amount),
													data.getDisplayName()
											)
									);
								} else {
									sender.sendMessage(String.format(BankConfig.Lang.CMD_ECO_TAKE_INSUFFICIENT_FUNDS, data.getDisplayName()));
								}
								break;
							case "set":
								Player player = Bukkit.getPlayer(data.getUuid());
								if(player != null){
									long inventoryBalance = data.getInventoryBalance();

									if(ItemUtil.inventoryWithdraw(player, (int) inventoryBalance) == -1){
										sender.sendMessage(BankConfig.Lang.INTERNAL_ERROR);

										return;
									}
								}

								data.setBankBalance(amount);
								data.markUnsaved();

								sender.sendMessage(
										String.format(
												BankConfig.Lang.CMD_ECO_SET_SUCCESSFUL,
												data.getDisplayName(),
												MsgUtil.formatCurrency(amount)
										)
								);

								if(player == null){
									sender.sendMessage(BankConfig.Lang.CMD_ECO_SET_OFFLINE_WARNING);
								}
								break;
						}

					});

				} catch (InterruptedException | ExecutionException | TimeoutException e) {
					sender.sendMessage(BankConfig.Lang.CMD_INTERNAL_ERROR);

					e.printStackTrace();
				}
			});
		} else {
			return BankConfig.Lang.CMD_ECO_USAGE;
		}

		return null;
	}
}
