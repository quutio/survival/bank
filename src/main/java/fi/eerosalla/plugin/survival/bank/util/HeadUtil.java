package fi.eerosalla.plugin.survival.bank.util;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.Base64;
import java.util.UUID;

public class HeadUtil {

	private static Field profileField;
	static {
		try {

			profileField = Class.forName("org.bukkit.craftbukkit.v1_18_R2.inventory.CraftMetaSkull").getDeclaredField("profile");
			profileField.setAccessible(true);

		} catch (NoSuchFieldException | ClassNotFoundException e) {
			e.printStackTrace();
		}

	}

	//https://www.spigotmc.org/threads/dynamic-player-head-textures.482194/#post-4047576
	public static ItemStack getHead(String textureUrl) {
		ItemStack item = new ItemStack(Material.PLAYER_HEAD, 1, (short) 3);

		if (textureUrl == null || textureUrl.isEmpty())
			return item;

		SkullMeta skullMeta = (SkullMeta) item.getItemMeta();
		GameProfile profile = new GameProfile(UUID.randomUUID(), null);

		byte[] encodedData = Base64.getEncoder().encode(String.format("{textures:{SKIN:{url:\"%s\"}}}", textureUrl).getBytes());
		profile.getProperties().put("textures", new Property("textures", new String(encodedData)));

		try {
			profileField.set(skullMeta, profile);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			e.printStackTrace();
		}

		item.setItemMeta(skullMeta);

		return item;
	}
}
