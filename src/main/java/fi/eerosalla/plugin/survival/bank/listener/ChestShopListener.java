package fi.eerosalla.plugin.survival.bank.listener;

import com.Acrobot.ChestShop.Events.ShopCreatedEvent;
import com.Acrobot.ChestShop.Events.ShopDestroyedEvent;
import fi.eerosalla.plugin.survival.bank.Bank;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public record ChestShopListener(Bank plugin) implements Listener {

	@EventHandler
	public void onShopCreate(ShopCreatedEvent event){
		if(event.getContainer() == null) return;

		plugin.chestShopService.addChestShop(event.getContainer().getLocation(), event.getPlayer().getUniqueId());
	}

	@EventHandler
	public void onShopDestroy(ShopDestroyedEvent event){
		if(event.getContainer() == null) return;

		plugin.chestShopService.removeChestShop(event.getContainer().getLocation());
	}

}
