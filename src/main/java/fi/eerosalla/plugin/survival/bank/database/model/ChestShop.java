package fi.eerosalla.plugin.survival.bank.database.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import lombok.*;

import java.util.UUID;

@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@DatabaseTable(tableName = "chestshops")
public class ChestShop {

	@Getter
	@DatabaseField(generatedId = true)
	private Integer id;

	@Getter
	@Setter
	@NonNull
	@DatabaseField(uniqueIndexName = "location_idx")
	private String worldName;

	@Getter
	@Setter
	@DatabaseField(uniqueIndexName = "location_idx")
	private int x, y, z;

	@Getter
	@Setter
	@DatabaseField
	private UUID uuid;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		ChestShop chestShop = (ChestShop) o;

		if (x != chestShop.x) return false;
		if (y != chestShop.y) return false;
		if (z != chestShop.z) return false;
		return worldName.equals(chestShop.worldName);
	}

	@Override
	public int hashCode() {
		int result = worldName.hashCode();
		result = 31 * result + x;
		result = 31 * result + y;
		result = 31 * result + z;
		return result;
	}

	public ChestShop clone(){
		return new ChestShop(-1, worldName, x, y, z, uuid);
	}
}
