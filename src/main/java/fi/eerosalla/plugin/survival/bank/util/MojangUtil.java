package fi.eerosalla.plugin.survival.bank.util;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

public class MojangUtil {

	public static UUID fetchUuidForName(String name){

		JSONObject mojangResponse;
		try {
			mojangResponse = new JSONObject(
					IOUtils.toString(
							new URL("https://api.mojang.com/users/profiles/minecraft/" + name),
							StandardCharsets.UTF_8
					)
			);
		} catch (IOException e) {
			e.printStackTrace();

			return null;
		}

		if(!mojangResponse.has("id")) return null;

		UUID uuid = null;
		try {
			uuid = UUID.fromString(mojangResponse.getString("id"));
		} catch (IllegalArgumentException ex){
			ex.printStackTrace();
		}

		return uuid;
	}

}
