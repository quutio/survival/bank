package fi.eerosalla.plugin.survival.bank.gui;

import lombok.Getter;

import java.util.List;

public abstract class GuiClickResponse {

	private GuiClickResponse(){}

	public abstract Type getType();

	public enum Type {
		NO_ACTION,
		UPDATE_SLOTS,
		CHANGE_VIEW,
		CLOSE_WINDOW
	}

	public static class NoAction extends GuiClickResponse {

		@Override
		public Type getType() {
			return Type.NO_ACTION;
		}

		private static NoAction INSTANCE;
		public static NoAction getInstance(){
			if(INSTANCE == null){
				INSTANCE = new NoAction();
			}

			return INSTANCE;
		}

	}

	public static class UpdateSlots extends GuiClickResponse {

		@Override
		public Type getType() {
			return Type.UPDATE_SLOTS;
		}

		@Getter
		private final List<Integer> slots;

		public UpdateSlots(List<Integer> slots){
			this.slots = slots;
		}

	}

	public static class ChangeView extends GuiClickResponse {

		@Override
		public Type getType() {
			return Type.CHANGE_VIEW;
		}

		@Getter
		private final String id;

		public ChangeView(String id){
			this.id = id;
		}

	}

	public static class CloseWindow extends GuiClickResponse {

		@Override
		public Type getType() {
			return Type.CLOSE_WINDOW;
		}

		private static CloseWindow INSTANCE;
		public static CloseWindow getInstance(){
			if(INSTANCE == null){
				INSTANCE = new CloseWindow();
			}

			return INSTANCE;
		}
	}

}
