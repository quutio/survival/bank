package fi.eerosalla.plugin.survival.bank.listener;

import fi.eerosalla.plugin.survival.bank.Bank;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public record PlayerListener(Bank plugin) implements Listener {

	@EventHandler
	public void onLogin(PlayerLoginEvent event) {

		if (!plugin.playerService.loadPlayerData(event.getPlayer())) {

			event.setResult(PlayerLoginEvent.Result.KICK_OTHER);
			event.setKickMessage(ChatColor.RED + "Database error. Please try logging in again.");

		}

	}

	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		plugin.playerService.scheduleSave(event.getPlayer().getUniqueId());

		plugin.guiManager.handleQuit(event.getPlayer());
	}

}
