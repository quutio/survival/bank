package fi.eerosalla.plugin.survival.bank.gui;

public enum InventoryClickType {
    PICKUP, QUICK_MOVE, SWAP, CLONE, THROW, QUICK_CRAFT, PICKUP_ALL
}
