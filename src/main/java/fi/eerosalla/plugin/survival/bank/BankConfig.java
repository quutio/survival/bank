package fi.eerosalla.plugin.survival.bank;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import net.md_5.bungee.api.ChatColor;
import org.spongepowered.configurate.CommentedConfigurationNode;
import org.spongepowered.configurate.ConfigurateException;
import org.spongepowered.configurate.ConfigurationOptions;
import org.spongepowered.configurate.yaml.NodeStyle;
import org.spongepowered.configurate.yaml.YamlConfigurationLoader;

import java.io.File;
import java.nio.file.Path;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BankConfig {

	public static class Database {

		protected static String HOST, DATABASE, USERNAME, PASSWORD;
		protected static Integer PORT;

		public static ConnectionSource getConnectionSource() throws SQLException {
			return new JdbcConnectionSource(
					"jdbc:mysql://" + HOST + ":" + PORT + "/" + DATABASE + "?user=" + USERNAME + "&password=" + PASSWORD + "&autoReconnect=true"
			);
		}
	}

	public static class Currency {

		public static String NAME;
		public static String NAME_PLURAL;

		public static String CHAT_COLOR;

	}

	public static class Lang {

		//General errors
		public static String INTERNAL_ERROR,
				INSUFFICIENT_FUNDS;

		//Command errors
		public static String CMD_NO_PERMISSION,
				CMD_INTERNAL_ERROR,
				CMD_PLAYER_NOT_FOUND,
				CMD_GAME_ONLY;

		//Pay command
		public static String CMD_PAY_USAGE,
			CMD_PAY_SENT,
			CMD_PAY_RECEIVED;

		//Balance command
		public static String CMD_BALANCE_USAGE,
				CMD_BALANCE_SELF_BALANCE,
				CMD_BALANCE_OTHER_BALANCE;

		//Baltop command
		public static String CMD_BALTOP_USAGE,
				CMD_BALTOP_HEADER,
				CMD_BALTOP_ENTRY,
				CMD_BALTOP_INVALID_PAGE;

		//Eco command
		public static String CMD_ECO_USAGE,
				CMD_ECO_FULL_USAGE,
				CMD_ECO_GIVE_SUCCESSFUL,
				CMD_ECO_TAKE_SUCCESSFUL,
				CMD_ECO_TAKE_INSUFFICIENT_FUNDS,
				CMD_ECO_SET_SUCCESSFUL,
				CMD_ECO_SET_OFFLINE_WARNING;

		//AccountView gui
		public static String GUI_ACCOUNTVIEW_TITLE,
				GUI_ACCOUNTVIEW_BALANCE,
				GUI_ACCOUNTVIEW_DEPOSIT,
				GUI_ACCOUNTVIEW_WITHDRAW;

	}

	private static final String DEFAULT_PREFIX = "&7[&aBank&7]&r ";

	private static final String COLOR_ERROR = "&#BD2626";
	private static final String COLOR_HIGHLIGHT = "&#77fc03";//;"&#3B82F6";

	public static void load(Bank plugin) {
		File dataFolder = plugin.getDataFolder();

		if (!dataFolder.exists()) {
			dataFolder.mkdirs();
		}

		YamlConfigurationLoader.Builder builder = YamlConfigurationLoader.builder();

		builder.path(Path.of(dataFolder.getPath(), "config.yml"));
		builder.nodeStyle(NodeStyle.BLOCK);

		ConfigurationOptions options = builder.defaultOptions();
		options.shouldCopyDefaults(true);
		builder.defaultOptions(options);

		YamlConfigurationLoader loader = builder.build();


		CommentedConfigurationNode root;
		try {
			root = loader.load();
		} catch (ConfigurateException ex) {
			plugin.getLogger().warning("Config load error");

			ex.printStackTrace();
			return;
		}

		/*
			Database
		 */
		Database.HOST = root.node("db", "host").getString("127.0.0.1");
		Database.PORT = root.node("db", "port").getInt(3306);
		Database.DATABASE = root.node("db", "database").getString("Bank");
		Database.USERNAME = root.node("db", "username").getString("bank");
		Database.PASSWORD = root.node("db", "password").getString("");

		/*
			Currency settings
		 */
		Currency.NAME = root.node("currency", "name").getString("Gnug");
		Currency.NAME_PLURAL = root.node("currency", "name_plural").getString("Gnugs");
		Currency.CHAT_COLOR = getColoredString(
				root.node("currency", "chat_color").getString("&#d4af37")
		);

		/*
			Error messages
		 */
		Lang.INTERNAL_ERROR = getColoredString(
				root.node("lang", "internal_error").getString(
						COLOR_ERROR + "Internal error occurred while trying to execute the given task."
				)
		);

		Lang.INSUFFICIENT_FUNDS = getColoredString(
			root.node("lang", "insufficient_funds").getString(
					COLOR_ERROR + "Insufficient funds."
			)
		);

		/*
			Command error messages
		 */
		Lang.CMD_NO_PERMISSION = getColoredString(
				root.node("lang", "cmd", "no_permission").getString(
						COLOR_ERROR + "You don't have permission to use this command!"
				)
		);

		Lang.CMD_INTERNAL_ERROR = getColoredString(
				root.node("lang", "cmd", "internal_error").getString(
						COLOR_ERROR + "Internal error occurred while trying to execute the command."
				)
		);
		Lang.CMD_PLAYER_NOT_FOUND = getColoredString(
				root.node("lang", "cmd", "player_not_found").getString(
						COLOR_ERROR + "Player not found."
				)
		);
		Lang.CMD_GAME_ONLY = getColoredString(
				root.node("lang", "cmd", "game_only").getString(
						COLOR_ERROR + "This command cannot be executed from console."
				)
		);

		/*
			Pay command
		 */
		Lang.CMD_PAY_USAGE = getColoredString(
				root.node("lang", "cmd", "pay", "usage").getString(
						COLOR_ERROR + "/%s <player> <amount>"
				)
		);
		Lang.CMD_PAY_SENT = getColoredString(
				root.node("lang", "cmd", "pay", "sent").getString(
						DEFAULT_PREFIX + "Successfully paid %s&r to player " + COLOR_HIGHLIGHT + "%s"
				)
		);
		Lang.CMD_PAY_RECEIVED = getColoredString(
				root.node("lang", "cmd", "pay", "receied").getString(
						DEFAULT_PREFIX + "You received %s&r from player " + COLOR_HIGHLIGHT + "%s"
				)
		);

		/*
			Balance command
		 */
		Lang.CMD_BALANCE_USAGE = getColoredString(
				root.node("lang", "cmd", "balance", "usage").getString(
						COLOR_ERROR + "/%s [player]"
				)
		);
		Lang.CMD_BALANCE_SELF_BALANCE = getColoredString(
				root.node("lang", "cmd", "balance", "self_balance").getString(
						DEFAULT_PREFIX + "Your balance is %s"
				)
		);
		Lang.CMD_BALANCE_OTHER_BALANCE = getColoredString(
				root.node("lang", "cmd", "balance", "other_balance").getString(
						DEFAULT_PREFIX + "The balance of player " + COLOR_HIGHLIGHT + "%s&r is %s"
				)
		);

		/*
			Baltop command
		 */
		Lang.CMD_BALTOP_USAGE = getColoredString(
				root.node("lang", "cmd", "baltop", "usage").getString(
					COLOR_ERROR + "/%s [page]"
				)
		);
		Lang.CMD_BALTOP_HEADER = getColoredString(
				root.node("lang", "cmd", "baltop", "header").getString(
					DEFAULT_PREFIX + "Baltop - page %d"
				)
		);
		Lang.CMD_BALTOP_ENTRY = getColoredString(
				root.node("lang", "cmd", "baltop", "entry").getString(
					"%d. %s&r - %s&r"
				)
		);
		Lang.CMD_BALTOP_INVALID_PAGE = getColoredString(
				root.node("lang", "cmd", "baltop", "invalid_page").getString(
						COLOR_ERROR + "Invalid page."
				)
		);

		/*
			Eco command
		 */
		Lang.CMD_ECO_USAGE = getColoredString(
				root.node("lang", "cmd", "eco", "usage").getString(
						COLOR_ERROR + "Invalid arguments. Use /eco without arguments to get a full list of available subcommands."
				)
		);
		Lang.CMD_ECO_FULL_USAGE = getColoredString(
				root.node("lang", "cmd", "eco", "full_usage").getString(
						DEFAULT_PREFIX + "Available subcommands:\n"
						+ "/eco give <player> <amount>\n"
						+ "/eco take <player> <amount>\n"
						+ "/eco set <player> <balance>"
				)
		);
		Lang.CMD_ECO_GIVE_SUCCESSFUL = getColoredString(
				root.node("lang", "cmd", "eco", "give_successful").getString(
					DEFAULT_PREFIX + "Successfully given %s&r to player %s"
				)
		);
		Lang.CMD_ECO_TAKE_SUCCESSFUL = getColoredString(
				root.node("lang", "cmd", "eco", "take_successful").getString(
					DEFAULT_PREFIX + "Successfully taken %s&r from player %s"
				)
		);
		Lang.CMD_ECO_TAKE_INSUFFICIENT_FUNDS = getColoredString(
			root.node("lang", "cmd", "eco", "take_insufficient_funds").getString(
					DEFAULT_PREFIX + "Error: The balance of player " + COLOR_HIGHLIGHT + "%s&r is less than given amount"
			)
		);
		Lang.CMD_ECO_SET_SUCCESSFUL = getColoredString(
				root.node("lang", "cmd", "eco", "set_successful").getString(
						DEFAULT_PREFIX + "Balance of player " + COLOR_HIGHLIGHT + "%s&r was successfully set to %s"
				)
		);
		Lang.CMD_ECO_SET_OFFLINE_WARNING = getColoredString(
			root.node("lang", "cmd", "eco", "set_offline_warning").getString(
				DEFAULT_PREFIX + "WARNING: Only bank balance is updated because player is offline"
			)
		);

		/*
			AccountView gui
		 */
		Lang.GUI_ACCOUNTVIEW_TITLE = getColoredString(
				root.node("lang", "gui", "accountview", "title").getString(
						"&8Bank"
				)
		);
		Lang.GUI_ACCOUNTVIEW_BALANCE = getColoredString(
				root.node("lang", "gui", "accountview", "balance").getString(
						"&rBalance: %s"
				)
		);
		Lang.GUI_ACCOUNTVIEW_DEPOSIT = getColoredString(
				root.node("lang", "gui", "accountview", "deposit").getString(
						"&cDeposit %s"
				)
		);
		Lang.GUI_ACCOUNTVIEW_WITHDRAW = getColoredString(
				root.node("lang", "gui", "accountview", "withdraw").getString(
						"&aWithdraw %s"
				)
		);


		try {
			loader.save(root);
		} catch (ConfigurateException e) {
			e.printStackTrace();
		}
	}

	private static final Pattern HEX_COLOR_PATTERN = Pattern.compile("^#(?:[0-9a-fA-F]{3}){1,2}$");

	private static ChatColor getColor(String colorStr) {
		ChatColor color;

		if (HEX_COLOR_PATTERN.matcher(colorStr).matches()) {
			color = ChatColor.of(colorStr);
		} else {
			color = ChatColor.WHITE;
		}

		return color;
	}

	private static final Pattern AMP_HEX_COLOR_PATTERN = Pattern.compile("&(#(?:[0-9a-fA-F]{3}){1,2})");

	private static String getColoredString(String input) {
		Matcher matcher = AMP_HEX_COLOR_PATTERN.matcher(input);

		StringBuilder builder = new StringBuilder();
		while (matcher.find()) {
			String hexColorStr = matcher.group(1);
			if (hexColorStr != null) {
				matcher.appendReplacement(builder, getColor(hexColorStr).toString());
			}
		}

		matcher.appendTail(builder);

		return ChatColor.translateAlternateColorCodes('&', builder.toString());
	}

}
