package fi.eerosalla.plugin.survival.bank.gui.views.components;

import fi.eerosalla.plugin.survival.bank.util.HeadUtil;
import lombok.Getter;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class Display {

	private static final HashMap<Character, ItemStack> HEADS = new HashMap<>();
	static {
		HEADS.put('\0', HeadUtil.getHead("http://textures.minecraft.net/texture/c723704a9d5910b9cd505dc99c779bf50379cb84745cc719e9f784dd8c"));

		HEADS.put('0', HeadUtil.getHead("http://textures.minecraft.net/texture/1f886d9c40ef7f50c238824792c41fbfb54f665f159bf1bcb0b27b3ead373b"));
		HEADS.put('1', HeadUtil.getHead("http://textures.minecraft.net/texture/a0a19e23d21f2db063cc55e99ae874dc8b23be779be34e52e7c7b9a25"));
		HEADS.put('2', HeadUtil.getHead("http://textures.minecraft.net/texture/cc596a41daea51be2e9fec7de2d89068e2fa61c9d57a8bdde44b55937b6037"));
		HEADS.put('3', HeadUtil.getHead("http://textures.minecraft.net/texture/b85d4fda56bfeb85124460ff72b251dca8d1deb6578070d612b2d3adbf5a8"));
		HEADS.put('4', HeadUtil.getHead("http://textures.minecraft.net/texture/3852a25fe69ca86fb982fb3cc7ac9793f7356b50b92cb0e193d6b4632a9bd629"));
		HEADS.put('5', HeadUtil.getHead("http://textures.minecraft.net/texture/74ee7d954eb14a5ccd346266231bf9a6716527b59bbcd7956cef04a9d9b"));
		HEADS.put('6', HeadUtil.getHead("http://textures.minecraft.net/texture/2682a3ae948374e037e3d7dd687d59d185dd2cc8fc09dfeb42f98f8d259e5c3"));
		HEADS.put('7', HeadUtil.getHead("http://textures.minecraft.net/texture/4ea30c24c60b3bc1af658ef661b771c48d5b9c9e28188cf9de9f832422e510"));
		HEADS.put('8', HeadUtil.getHead("http://textures.minecraft.net/texture/66abafd023f230e4485aaf26e19368f5980d4f14a59fcc6d11a4466994892"));
		HEADS.put('9', HeadUtil.getHead("http://textures.minecraft.net/texture/8d7910e10334f890a625483ac0c824b5e4a1a4b15a956327a3e3ae458d9ea4"));

		HEADS.values().forEach((item) -> {
			ItemMeta meta = item.getItemMeta();

			meta.setDisplayName(ChatColor.WHITE.toString());

			item.setItemMeta(meta);
		});

	}

	@Getter
	private final int offset, length;

	@Getter
	private final char emptyChar;
	private final ItemStack[] itemArr;
	private final char[] charArr;
	public Display(int x, int y, int length, char emptyChar, ItemStack[] itemArr){
		this.offset = y * 9 + x;
		this.length = length;
		this.emptyChar = emptyChar;
		this.itemArr = itemArr;
		this.charArr = new char[length];

		ItemStack emptyItem = HEADS.get(emptyChar);
		if(emptyItem != null) {
			for (int i = 0; i < length; ++i) {
				itemArr[i + offset] = emptyItem;
			}
		}
	}

	public List<Integer> setValue(String str){
		ArrayList<Integer> updated = new ArrayList<>();

		if(str.length() > length){
			return updated;
		}

		char[] tempArr = str.toCharArray();
		for(int i = 0; i < length; ++i){
			int charArrIndex = length - 1 - i;
			int tempArrIndex = tempArr.length - 1 - i;


			if(tempArrIndex < 0){
				if(charArr[charArrIndex] != '\0'){
					updated.add(charArrIndex);

					setChar(charArrIndex, '\0');
				}
			} else {
				if(tempArr[tempArrIndex] != charArr[charArrIndex]){
					updated.add(charArrIndex);

					setChar(charArrIndex, tempArr[tempArrIndex]);
				}
			}
		}

		return updated.stream().mapToInt(i -> i + offset).boxed().collect(Collectors.toList());
	}

	private void setChar(int index, char c){
		charArr[index] = c;

		if(c == '\0'){
			ItemStack item = HEADS.get(emptyChar);

			if(item == null) item = new ItemStack(Material.AIR);

			itemArr[index + offset] = item;
		} else {
			ItemStack item = HEADS.get(c);

			if(item != null){

				itemArr[index + offset] = item;

			}
		}
	}

}
