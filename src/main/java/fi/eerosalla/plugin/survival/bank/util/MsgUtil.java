package fi.eerosalla.plugin.survival.bank.util;

import fi.eerosalla.plugin.survival.bank.BankConfig;

public class MsgUtil {

	public static String formatCurrency(long amount){
		return formatCurrency(amount, true);
	}
	public static String formatCurrency(long amount, boolean color){
		StringBuilder builder = new StringBuilder();

		if(color){
			builder.append(BankConfig.Currency.CHAT_COLOR);
		}

		builder.append(amount);
		builder.append(' ');

		if(amount == 1){
			builder.append(BankConfig.Currency.NAME);
		} else {
			builder.append(BankConfig.Currency.NAME_PLURAL);
		}

		return builder.toString();
	}
}
