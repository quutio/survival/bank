package fi.eerosalla.plugin.survival.bank;

import fi.eerosalla.plugin.survival.bank.database.model.BankPlayer;
import fi.eerosalla.plugin.survival.bank.util.MsgUtil;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.economy.EconomyResponse;
import org.bukkit.OfflinePlayer;

import java.util.List;

public record VaultConnector(Bank plugin) implements Economy {

	private final static EconomyResponse ERROR_RESPONSE = new EconomyResponse(
			0.0D,
			0.0D,
			EconomyResponse.ResponseType.FAILURE,
			BankConfig.Lang.INTERNAL_ERROR
	);

	@Override
	public boolean isEnabled() {
		return true;
	}

	@Override
	public String getName() {
		return "Bank";
	}

	@Override
	public boolean hasBankSupport() {
		return false;
	}

	@Override
	public int fractionalDigits() {
		return 0;
	}

	@Override
	public String format(double amount) {
		return MsgUtil.formatCurrency((long) amount);
	}

	@Override
	public String currencyNamePlural() {
		return BankConfig.Currency.NAME_PLURAL;
	}

	@Override
	public String currencyNameSingular() {
		return BankConfig.Currency.NAME;
	}

	@Override
	public boolean hasAccount(String playerName) {
		return true;
	}

	@Override
	public boolean hasAccount(OfflinePlayer player) {
		return true;
	}

	@Override
	public boolean hasAccount(String playerName, String worldName) {
		return true;
	}

	@Override
	public boolean hasAccount(OfflinePlayer player, String worldName) {
		return true;
	}

	@Override
	public double getBalance(String playerName) {
		plugin.getLogger().warning("Unknown plugin tried to use getBalance(name)");
		return 0.0D;
	}

	@Override
	public double getBalance(OfflinePlayer player) {
		BankPlayer data = plugin.playerService.getPlayerFromCache(player.getUniqueId());
		if (data == null){
			return 0.0D;
		}

		return data.getBalance();
	}

	@Override
	public double getBalance(String playerName, String world) {
		plugin.getLogger().warning("Unknown plugin tried to use getBalance(name, world)");
		return 0;
	}

	@Override
	public double getBalance(OfflinePlayer player, String world) {
		return getBalance(player);
	}

	@Override
	public boolean has(String playerName, double amount) {
		plugin.getLogger().warning("Unknown plugin tried to use has(name, amount)");
		return false;
	}

	@Override
	public boolean has(OfflinePlayer player, double amount) {
		BankPlayer data = plugin.playerService.getPlayerFromCache(player.getUniqueId());
		if (data == null) return false;

		return data.getBalance() >= (long) Math.ceil(amount);
	}

	@Override
	public boolean has(String playerName, String worldName, double amount) {
		plugin.getLogger().warning("Unknown plugin tried to use has(name, world, amount)");
		return false;
	}

	@Override
	public boolean has(OfflinePlayer player, String worldName, double amount) {
		return has(player, amount);
	}

	@Override
	public EconomyResponse withdrawPlayer(String playerName, double amount) {
		plugin.getLogger().warning("Unknown plugin tried to use withdrawPlayer(name, amount)");
		return null;
	}

	@Override
	public EconomyResponse withdrawPlayer(OfflinePlayer player, double amount) {
		if (amount % 1 != 0) return ERROR_RESPONSE;

		BankPlayer data = plugin.playerService.getPlayerFromCache(player.getUniqueId());
		if (data == null) return ERROR_RESPONSE;

		long longAmount = (long) amount; //already checked??
		if (data.getBalance() < longAmount) {
			return new EconomyResponse(0.0D, data.getBalance(), EconomyResponse.ResponseType.FAILURE, null);
		}

		if (!data.withdraw(longAmount)) {
			return ERROR_RESPONSE;
		}

		return new EconomyResponse((double) -longAmount, data.getBalance(), EconomyResponse.ResponseType.SUCCESS, null);
	}

	@Override
	public EconomyResponse withdrawPlayer(String playerName, String worldName, double amount) {
		plugin.getLogger().warning("Unknown plugin tried to use withdrawPlayer(name, world, amount)");
		return null;
	}

	@Override
	public EconomyResponse withdrawPlayer(OfflinePlayer player, String worldName, double amount) {
		return withdrawPlayer(player, amount);
	}

	@Override
	public EconomyResponse depositPlayer(String playerName, double amount) {
		plugin.getLogger().warning("Unknown plugin tried to use depositPlayer(name, amount)");
		return null;
	}

	@Override
	public EconomyResponse depositPlayer(OfflinePlayer player, double amount) {
		if (amount % 1.0D != 0) return ERROR_RESPONSE;

		BankPlayer data = plugin.playerService.getPlayerFromCache(player.getUniqueId());
		if (data == null) return ERROR_RESPONSE;

		long longAmount = (long) amount;
		data.deposit(longAmount);

		return new EconomyResponse(longAmount, data.getBalance(), EconomyResponse.ResponseType.SUCCESS, "");
	}

	@Override
	public EconomyResponse depositPlayer(String playerName, String worldName, double amount) {
		plugin.getLogger().warning("Unknown plugin tried to use depositPlayer(name, world, amount)");
		return null;
	}

	@Override
	public EconomyResponse depositPlayer(OfflinePlayer player, String worldName, double amount) {
		return depositPlayer(player, amount);
	}

	@Override
	public EconomyResponse createBank(String name, String player) {
		return null;
	}

	@Override
	public EconomyResponse createBank(String name, OfflinePlayer player) {
		return null;
	}

	@Override
	public EconomyResponse deleteBank(String name) {
		return null;
	}

	@Override
	public EconomyResponse bankBalance(String name) {
		return null;
	}

	@Override
	public EconomyResponse bankHas(String name, double amount) {
		return null;
	}

	@Override
	public EconomyResponse bankWithdraw(String name, double amount) {
		return null;
	}

	@Override
	public EconomyResponse bankDeposit(String name, double amount) {
		return null;
	}

	@Override
	public EconomyResponse isBankOwner(String name, String playerName) {
		return null;
	}

	@Override
	public EconomyResponse isBankOwner(String name, OfflinePlayer player) {
		return null;
	}

	@Override
	public EconomyResponse isBankMember(String name, String playerName) {
		return null;
	}

	@Override
	public EconomyResponse isBankMember(String name, OfflinePlayer player) {
		return null;
	}

	@Override
	public List<String> getBanks() {
		return null;
	}

	@Override
	public boolean createPlayerAccount(String playerName) {
		return true;
	}

	@Override
	public boolean createPlayerAccount(OfflinePlayer player) {
		return true;
	}

	@Override
	public boolean createPlayerAccount(String playerName, String worldName) {
		return true;
	}

	@Override
	public boolean createPlayerAccount(OfflinePlayer player, String worldName) {
		return true;
	}
}
