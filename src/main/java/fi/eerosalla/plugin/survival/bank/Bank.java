package fi.eerosalla.plugin.survival.bank;

import com.j256.ormlite.support.ConnectionSource;
import fi.eerosalla.plugin.survival.bank.commands.BankCommand;
import fi.eerosalla.plugin.survival.bank.database.ChestShopService;
import fi.eerosalla.plugin.survival.bank.database.PlayerService;
import fi.eerosalla.plugin.survival.bank.gui.BankGui;
import fi.eerosalla.plugin.survival.bank.gui.GuiManager;
import fi.eerosalla.plugin.survival.bank.listener.ChestShopListener;
import fi.eerosalla.plugin.survival.bank.listener.PlayerListener;
import fi.eerosalla.plugin.survival.bank.util.SchedulerUtil;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.plugin.ServicePriority;
import java.sql.SQLException;
import org.bukkit.plugin.java.JavaPlugin;


public final class Bank extends JavaPlugin {

	public PlayerService playerService;
	public ChestShopService chestShopService;
	public GuiManager guiManager;

	@Override
	public void onEnable() {
		BankConfig.load(this);
		SchedulerUtil.init(this);

		try {

			ConnectionSource connectionSource = BankConfig.Database.getConnectionSource();

			playerService = new PlayerService(this, connectionSource);
			chestShopService = new ChestShopService(this, connectionSource);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		getCommand("money").setExecutor(new BankCommand(this));

		getServer().getPluginManager().registerEvents(new PlayerListener(this), this);

		if(getServer().getPluginManager().getPlugin("ChestShop") != null) {
			getLogger().info("ChestShop plugin found. Support for offline player shops enabled.");

			getServer().getPluginManager().registerEvents(new ChestShopListener(this), this);
		} else {
			getLogger().warning("ChestShop plugin not found.");
		}

		getServer().getServicesManager().register(Economy.class, new VaultConnector(this), this, ServicePriority.Highest);

		guiManager = new GuiManager(this);
		BankGui.registerGenerators(this, guiManager);
	}

	@Override
	public void onDisable(){
		getServer().getScheduler().cancelTasks(this);

		playerService.saveAll();
		chestShopService.saveAll();
	}

}
