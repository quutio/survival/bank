package fi.eerosalla.plugin.survival.bank.gui.views;

import fi.eerosalla.plugin.survival.bank.Bank;
import fi.eerosalla.plugin.survival.bank.BankConfig;
import fi.eerosalla.plugin.survival.bank.database.model.BankPlayer;
import fi.eerosalla.plugin.survival.bank.gui.GuiClickResponse;
import fi.eerosalla.plugin.survival.bank.gui.GuiView;
import fi.eerosalla.plugin.survival.bank.gui.views.components.Display;
import fi.eerosalla.plugin.survival.bank.util.ItemUtil;
import fi.eerosalla.plugin.survival.bank.util.MsgUtil;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AccountView extends GuiView {

	private static final ItemStack[] template;
	static {
		template = new ItemStack[9 * 5];

		ItemStack redPane = ItemUtil.getItemStack(Material.RED_STAINED_GLASS_PANE, "&c");
		ItemStack greenPane =  ItemUtil.getItemStack(Material.LIME_STAINED_GLASS_PANE, "&c");
		ItemStack grayPane = ItemUtil.getItemStack(Material.GRAY_STAINED_GLASS_PANE, "&c");
		ItemStack air = new ItemStack(Material.AIR);

		for(int x = 0; x < 9; ++x){
			template[x] = grayPane;
			template[x+9] = (x == 0 || x == 8)? grayPane : air;

			for(int y = 0; y < 3; ++y){
				ItemStack item = null;

				if(x == 0 || x == 8 || y == 0 || y == 2){

					if(x == 4){
						item = grayPane;
					} else if(x < 4){
						item = redPane;
					} else {
						item = greenPane;
					}

				} else {

					switch(x){
						case 1:
							item = ItemUtil.getItemStack(Material.GOLD_NUGGET, String.format(BankConfig.Lang.GUI_ACCOUNTVIEW_DEPOSIT, MsgUtil.formatCurrency(1)));
							break;
						case 2:
							item = ItemUtil.getItemStack(Material.GOLD_INGOT, String.format(BankConfig.Lang.GUI_ACCOUNTVIEW_DEPOSIT, MsgUtil.formatCurrency(9)));
							break;
						case 3:
							item = ItemUtil.getItemStack(Material.GOLD_BLOCK, String.format(BankConfig.Lang.GUI_ACCOUNTVIEW_DEPOSIT, MsgUtil.formatCurrency(81)));
							break;
						case 4:
							item = ItemUtil.getItemStack(Material.CHEST, "&r");
							break;
						case 5:
							item = ItemUtil.getItemStack(Material.GOLD_BLOCK, String.format(BankConfig.Lang.GUI_ACCOUNTVIEW_WITHDRAW, MsgUtil.formatCurrency(81)));
							break;
						case 6:
							item = ItemUtil.getItemStack(Material.GOLD_INGOT, String.format(BankConfig.Lang.GUI_ACCOUNTVIEW_WITHDRAW, MsgUtil.formatCurrency(9)));
							break;
						case 7:
							item = ItemUtil.getItemStack(Material.GOLD_NUGGET, String.format(BankConfig.Lang.GUI_ACCOUNTVIEW_WITHDRAW, MsgUtil.formatCurrency(1)));
					}

				}

				template[(y + 2)*9+x] = item;

			}
		}
	}

	private final ItemStack[] items;

	private final Display display;

	private final Bank plugin;
	private final UUID uuid;
	public AccountView(int windowId, Bank plugin, UUID uuid){
		super(windowId);

		this.plugin = plugin;
		this.uuid = uuid;

		items = ItemUtil.cloneTemplate(template);

		this.display = new Display(1, 1, 7, '\0', items);

		updateBalance();

	}

	private GuiClickResponse updateBalance(){
		BankPlayer data = plugin.playerService.getPlayerFromCache(this.uuid);

		if(data == null){
			return GuiClickResponse.NoAction.getInstance();
		}

		long balance = data.getBankBalance();

		ArrayList<Integer> updatedSlots = new ArrayList<>();
		updatedSlots.add(31);

		items[31] = ItemUtil.getItemStack(
				Material.CHEST,
				String.format(BankConfig.Lang.GUI_ACCOUNTVIEW_BALANCE, MsgUtil.formatCurrency(balance))
		);

		updatedSlots.addAll(display.setValue("" + balance));

		return new GuiClickResponse.UpdateSlots(updatedSlots);
	}

	@Override
	public String getTitle(){
		return BankConfig.Lang.GUI_ACCOUNTVIEW_TITLE;
	}

	@Override
	public GuiClickResponse onClick(int x, boolean shift) {

		if(27 < x && x < 36){
			x -= 28;

			int amount;
			switch(x){
				case 0:
					amount = 1;
					break;
				case 1:
					amount = 9;
					break;
				case 2:
					amount = 81;
					break;
				case 4:
					amount = -81;
					break;
				case 5:
					amount = -9;
					break;
				case 6:
					amount = -1;
					break;
				default:
					return GuiClickResponse.NoAction.getInstance();
			}

			if(shift){
				amount *= 64;
			}

			BankPlayer data = plugin.playerService.getPlayerFromCache(this.uuid);
			if(data == null) return GuiClickResponse.NoAction.getInstance();

			Player player = Bukkit.getPlayer(data.getUuid());
			if(player == null){
				//Ukn error

				return GuiClickResponse.NoAction.getInstance();
			}

			if(amount > 0){ //Deposit (positive amount)

				long inventoryBalance = data.getInventoryBalance();
				if(inventoryBalance < amount){
					//not enough items

					return GuiClickResponse.NoAction.getInstance();
				}

				int leftOver = ItemUtil.inventoryWithdraw(player, amount);
				if(leftOver == -1){
					player.sendMessage(BankConfig.Lang.INTERNAL_ERROR);

					return GuiClickResponse.NoAction.getInstance();
				}

				data.setBankBalance(data.getBankBalance() + amount + leftOver);


			} else { //Withdraw (negative amount)

				if(data.getBankBalance() < -amount){
					//Insufficient funds

					return GuiClickResponse.NoAction.getInstance();
				}

				List<ItemStack> items = ItemUtil.getAsItems(-amount);
				if(items == null){
					player.sendMessage(BankConfig.Lang.INTERNAL_ERROR);

					return GuiClickResponse.NoAction.getInstance();
				}

				if(!ItemUtil.inventoryDeposit(player, -amount)){
					//No space

					return GuiClickResponse.NoAction.getInstance();
				}

				data.setBankBalance(data.getBankBalance() + amount);

			}

			data.markUnsaved();

			return updateBalance();
		}

		return GuiClickResponse.NoAction.getInstance();
	}

	@Override
	public String getType(){
		return "generic_9x5";
	}

	@Override
	public ItemStack[] getItems() {
		return items;
	}

}
