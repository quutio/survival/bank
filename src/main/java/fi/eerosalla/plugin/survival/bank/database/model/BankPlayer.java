package fi.eerosalla.plugin.survival.bank.database.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import fi.eerosalla.plugin.survival.bank.util.ItemUtil;
import fi.eerosalla.plugin.survival.bank.util.SchedulerUtil;
import lombok.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@DatabaseTable(tableName = "players")
public class BankPlayer {

	@Getter
	@NonNull
	@DatabaseField(id = true)
	private UUID uuid;

	@Getter
	@Setter
	@DatabaseField(index = true)
	private String name;

	@Getter
	@Setter
	@DatabaseField(columnName = "balance")
	private long bankBalance;

	public String getDisplayName(){
		Player player = Bukkit.getPlayer(this.getUuid());

		return player == null? this.getName() : player.getName();
	}

	@Getter
	private volatile long keepUntil = 0;

	public void setKeep(int seconds){
		long newKeepUntil = System.currentTimeMillis() + seconds * 1000L;

		if(newKeepUntil > keepUntil){
			keepUntil = newKeepUntil;
		}
	}

	private boolean saved = true;

	public void markUnsaved(){
		saved = false;
	}

	//Return true if database record isn't up-to-date and update is needed
	public boolean save(){
		if(!saved){
			saved = true;

			return true;
		}

		return false;
	}

	public long getBalance(){
		return this.getBankBalance() + getInventoryBalance();
	}

	public long getInventoryBalance(){
		//Return 0 if called outside the main thread
		if(!SchedulerUtil.isSync()){
			return 0L;
		}

		//Can't fetch inventory if player isn't online
		Player player = Bukkit.getPlayer(this.getUuid());
		if(player == null){
			return 0L;
		}

		return ItemUtil.getInventoryBalance(player.getInventory());
	}

	public void deposit(long amount){
		if(amount < 0) return;

		setBankBalance(getBankBalance() + amount);

		this.markUnsaved();
	}

	public boolean withdraw(long amount){
		if(amount < 0) return false;

		long bankBalance = getBankBalance();
		long inventoryBalance = getInventoryBalance();

		long fullBalance = bankBalance + inventoryBalance;
		if(fullBalance < amount){
			return false;
		}

		Player player = Bukkit.getPlayer(this.getUuid());
		if(player == null){
			return false;
		}

		long amountFromInventory = Math.min(amount, inventoryBalance);
		if(amountFromInventory >= Integer.MAX_VALUE){
			return false;
		}

		int leftOver = 0;
		if(amountFromInventory != 0 &&
				(leftOver = ItemUtil.inventoryWithdraw(player, (int) amountFromInventory)) == -1){
			return false;
		}

		amount -= amountFromInventory;
		this.setBankBalance(this.getBankBalance() - amount + leftOver);

		this.markUnsaved();

		return true;
	}

	@Override
	public BankPlayer clone(){
		return new BankPlayer(this.getUuid(), this.getName(), this.getBankBalance());
	}

	public BankPlayer(UUID uuid, String name, long balance){
		this(uuid, name, balance, 0L, true);
	}
}
