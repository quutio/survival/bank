package fi.eerosalla.plugin.survival.bank.util;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.reflect.StructureModifier;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PacketUtil {

	public static PacketContainer getWindowOpenPacket(int windowId, String type, String title){

		int typeId;
		switch(type){
			case "generic_9x1":
				typeId = 0;
				break;
			case "generic_9x2":
				typeId = 1;
				break;
			case "generic_9x3":
				typeId = 2;
				break;
			case "generic_9x4":
				typeId = 3;
				break;
			case "generic_9x5":
				typeId = 4;
				break;
			default: //generic_9x6
				typeId = 5;
		}

		PacketContainer packet = new PacketContainer(PacketType.Play.Server.OPEN_WINDOW);

		StructureModifier<Integer> integers = packet.getIntegers();
		integers.write(0, windowId);
		integers.write(1, typeId);

		StructureModifier<WrappedChatComponent> chatComponents = packet.getChatComponents();
		chatComponents.write(0, WrappedChatComponent.fromText(title));

		return packet;
	}

	public static PacketContainer getWindowItemsPacket(int windowId, ItemStack[] items){

		PacketContainer packet = new PacketContainer(PacketType.Play.Server.WINDOW_ITEMS);

		StructureModifier<Integer> integers = packet.getIntegers();
		integers.write(0, windowId);
		integers.write(1, 0);

		StructureModifier<List<ItemStack>> itemLists = packet.getItemListModifier();
		itemLists.write(0, Arrays.stream(items).collect(Collectors.toList()));

		return packet;
	}

	public static PacketContainer getSetSlotPacket(int windowId, int slotId, ItemStack item){

		PacketContainer packet = new PacketContainer(PacketType.Play.Server.SET_SLOT);

		StructureModifier<Integer> integers = packet.getIntegers();
		integers.write(0, windowId);
		integers.write(2, slotId);

		packet.getItemModifier().write(0, item);

		return packet;
	}

	public static PacketContainer getWindowClosePacket(int windowId){

		PacketContainer packet = new PacketContainer(PacketType.Play.Server.CLOSE_WINDOW);

		packet.getIntegers().write(0, windowId);

		return packet;
	}

}
