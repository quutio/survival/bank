package fi.eerosalla.plugin.survival.bank.util;

import fi.eerosalla.plugin.survival.bank.Bank;
import org.bukkit.plugin.Plugin;

public class SchedulerUtil {

	private static Plugin PLUGIN;
	private static long MAIN_THREAD_ID;

	public static void init(Bank plugin){
		PLUGIN = plugin;
		MAIN_THREAD_ID = Thread.currentThread().getId();
	}

	public static void sync(Runnable runnable) { PLUGIN.getServer().getScheduler().runTask(PLUGIN, runnable); }

	public static void async(Runnable runnable) { PLUGIN.getServer().getScheduler().runTaskAsynchronously(PLUGIN, runnable); }

	public static boolean isSync(){ return Thread.currentThread().getId() == MAIN_THREAD_ID; }
}
