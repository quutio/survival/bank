package fi.eerosalla.plugin.survival.bank.util;

import lombok.NonNull;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ItemUtil {

	private static final int MAX_INVENTORY_BALANCE = 4 * 9 * 64 * 9 * 9; //4x9 inventory full of gold blocks

	public static ArrayList<ItemStack> getAsItems(int amount) {

		if (amount > MAX_INVENTORY_BALANCE) return null;

		final ArrayList<ItemStack> list = new ArrayList<>();

		int blocks = amount / 81;
		amount -= blocks * 81;

		int ingots = amount / 9;
		amount -= ingots * 9;

		int nuggets = amount;

		while (blocks > 0) {
			int stack = blocks;
			if (stack > 64) stack = 64;

			list.add(new ItemStack(Material.GOLD_BLOCK, stack));

			blocks -= stack;
		}

		while (ingots > 0) {
			int stack = ingots;
			if (stack > 64) stack = 64;

			list.add(new ItemStack(Material.GOLD_INGOT, stack));

			ingots -= stack;
		}

		while (nuggets > 0) {
			int stack = nuggets;
			if (stack > 64) stack = 64;

			list.add(new ItemStack(Material.GOLD_NUGGET, stack));

			nuggets -= stack;
		}

		Collections.reverse(list);

		return list;
	}

	public static int inventoryWithdraw(Player player, int amount) {

		Inventory inventory = player.getInventory();

		Map.Entry<List<Integer>, Integer> slotsAndBalance = getUsableSlotsAndBalance(inventory);
		List<Integer> slots = slotsAndBalance.getKey();
		Integer balance = slotsAndBalance.getValue();

		if (balance < amount) {
			return -1;
		}

		int newBalance = balance - amount;

		List<ItemStack> replacementItems = getAsItems(newBalance);
		if (replacementItems == null) {
			return -1;
		}

		insertReplacementItems(player, inventory, slots, replacementItems);

		return replacementItems.stream().mapToInt(ItemUtil::getItemValue).sum();
	}

	private static void insertReplacementItems(Player player, Inventory inventory, List<Integer> slots,
	                                           List<ItemStack> replacementItems) {
		slots.stream()
				.map((slot) ->
						Map.entry(
								slot,
								replacementItems.isEmpty() ?
										new ItemStack(Material.AIR) :
										replacementItems.remove(replacementItems.size() - 1)
						)
				)
				.forEach((slotEntry) -> inventory.setItem(slotEntry.getKey(), slotEntry.getValue()));

		player.updateInventory();
	}

	private static final ItemStack AIR = new ItemStack(Material.AIR);

	private static Map.Entry<List<Integer>, Integer> getUsableSlotsAndBalance(Inventory inventory) {

		return IntStream.range(0, 36)
				.boxed()
				.map(i -> Map.entry(i, Objects.requireNonNullElse(inventory.getItem(i), AIR)))
				.collect(
						Collectors.teeing(
								Collectors.filtering(e -> e.getValue().getType().equals(Material.AIR), //Get empty
										// slots
										Collectors.mapping( //Map to list of slot numbers
												Map.Entry::getKey,
												Collectors.toList()
										)
								),

								Collectors.mapping( //Map to (slot, itemValue) pairs
										e -> Map.entry(e.getKey(), getItemValue(e.getValue())),

										Collectors.filtering( //Get items with value greater than 0
												e -> e.getValue() > 0,
												Collectors.teeing(
														//Map to list of slot numbers
														Collectors.mapping(Map.Entry::getKey, Collectors.toList()),

														//Sum all item values
														Collectors.summingInt(Map.Entry::getValue),
														Map::entry
												)
										)
								),

								(list, listAndSum) -> Map.entry(
										Stream.of(listAndSum.getKey(), list)
												.flatMap(List::stream)
												.collect(Collectors.toList()),
										listAndSum.getValue()
								)
						)
				);

	}

	private static int getItemValue(ItemStack item) {
		if (item == null) return 0;
		int value = item.getAmount();

		switch (item.getType()) {
			case GOLD_NUGGET:
				break;
			case GOLD_INGOT:
				value *= 9;
				break;
			case GOLD_BLOCK:
				value *= 81;
				break;
			default:
				return 0;
		}

		if (!item.hasItemMeta()) {
			return value;
		}

		ItemMeta meta = item.getItemMeta();
		if (!item.getEnchantments().isEmpty() || meta.hasDisplayName() || meta.hasLore()) {
			return 0;
		}

		return value;
	}

	public static boolean inventoryDeposit(Player player, int amount) {

		Inventory inventory = player.getInventory();

		Map.Entry<List<Integer>, Integer> slotsAndBalance = getUsableSlotsAndBalance(inventory);
		List<Integer> slots = slotsAndBalance.getKey();
		Integer balance = slotsAndBalance.getValue();

		int newBalance = balance + amount;

		List<ItemStack> replacementItems = getAsItems(newBalance);
		if (replacementItems == null || replacementItems.size() > slots.size()) {
			return false;
		}

		insertReplacementItems(player, inventory, slots, replacementItems);

		return true;
	}

	public static int getInventoryBalance(Inventory inventory) {
		return IntStream.range(0, 36)
				.boxed()
				.map(inventory::getItem)
				.filter(Objects::nonNull)
				.mapToInt(ItemUtil::getItemValue)
				.sum();
	}

	public static ItemStack getItemStack(Material material, String name) {
		return getItemStack(material, name, null);
	}

	public static ItemStack getItemStack(Material material, String name, String lore) {
		ItemStack item = new ItemStack(material);
		ItemMeta meta = item.getItemMeta();

		if (name != null) {
			name = ChatColor.translateAlternateColorCodes('&', name);

			meta.setDisplayName(name);
		}

		if (lore != null) {
			lore = ChatColor.translateAlternateColorCodes('&', lore);

			meta.setLore(Arrays.stream(lore.split("\n")).toList());
		}

		item.setItemMeta(meta);

		return item;
	}

	public static ItemStack[] cloneTemplate(ItemStack[] template) {
		ItemStack[] arr = new ItemStack[template.length];

		for (int i = 0; i < template.length; ++i) {
			if (template[i] != null) {
				arr[i] = template[i].clone();
			}
		}

		return arr;
	}

}
