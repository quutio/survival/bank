package fi.eerosalla.plugin.survival.bank.database;

import com.google.common.collect.HashBasedTable;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import fi.eerosalla.plugin.survival.bank.Bank;
import fi.eerosalla.plugin.survival.bank.database.model.ChestShop;
import fi.eerosalla.plugin.survival.bank.util.HashMapSet;
import org.bukkit.*;
import org.bukkit.block.Block;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

import static fi.eerosalla.plugin.survival.bank.util.SchedulerUtil.*;

public class ChestShopService {

	private Dao<ChestShop, UUID> chestShopDao;

	//New ChestShop changes to be saved to the database
	private final HashSet<ChestShop> updateQueue = new HashSet<>();

	//Used for querying whether the player has shops or not. Players with shops are required to be cached at all times even when they are offline
	private final HashMap<UUID, HashSet<ChestShop>> uuidToShopCache = new HashMap<>();

	//Used for querying the old owner UUID when creating/replacing a shop
	private final HashMapSet<ChestShop> allShopsCache = new HashMapSet<>();

	public Bank plugin;
	public ChestShopService(Bank plugin, ConnectionSource connectionSource){
		this.plugin = plugin;

		try {
			chestShopDao = DaoManager.createDao(connectionSource, ChestShop.class);

			TableUtils.createTableIfNotExists(connectionSource, ChestShop.class);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		try {
			//Fetch all shops to cache (+ remove invalid shops)
			List<ChestShop> chestShops = removeMissingShops(chestShopDao.queryForAll());
			chestShops.forEach(allShopsCache::add);

			//Form uuid query cache from shop cache
			allShopsCache.values().forEach((data) ->
					uuidToShopCache.computeIfAbsent(
							data.getUuid(),
							(u) -> new HashSet<>()
					).add(data)
			);

			//Load all players who have shops to econ cache
			uuidToShopCache.keySet().forEach((uuid) -> plugin.playerService.loadPlayerData(uuid));
		} catch (SQLException e) {
			e.printStackTrace();
		}



		plugin.getServer().getScheduler().runTaskTimer(plugin, () -> {

			if(updateQueue.isEmpty()) return;

			final List<ChestShop> updatedObjects = updateQueue.stream().toList();
			updateQueue.clear();

			updatedObjects.forEach((data) -> {

				ChestShop oldEntry = allShopsCache.remove(data);
				if(oldEntry != null){
					UUID oldUuid = oldEntry.getUuid();

					if(oldUuid != data.getUuid()){
						uuidToShopCache.computeIfPresent(oldUuid, (uuid, set) -> {
							set.remove(data);

							return set.isEmpty()? null : set;
						});
					}
				}

				if(data.getUuid() != null){
					uuidToShopCache.computeIfAbsent(data.getUuid(), (u) -> {
						//Load player to econ cache
						async(() -> plugin.playerService.loadPlayerData(data.getUuid()));

						return new HashSet<>();
					}).add(data);

					allShopsCache.add(data);
				}

			});

			final List<ChestShop> updatedObjectsClone = updatedObjects.stream().map(ChestShop::clone).collect(Collectors.toList());

			async(() -> save(updatedObjectsClone));

		}, 0L, 1200L);
	}

	private final static HashSet<Material> CONTAINER_MATERIALS = new HashSet<>();
	static {
		CONTAINER_MATERIALS.add(Material.CHEST);
		CONTAINER_MATERIALS.add(Material.TRAPPED_CHEST);
		CONTAINER_MATERIALS.add(Material.BARREL);
	}

	private List<ChestShop> removeMissingShops(List<ChestShop> input){
		HashSet<ChestShop> toBeRemoved = new HashSet<>();

		HashBasedTable<Integer, Integer, HashMap<String, ArrayList<ChestShop>>> table = HashBasedTable.create();

		input.forEach((data) -> {
			int cx = data.getX() >> 4;
			int cz = data.getZ() >> 4;

			HashMap<String, ArrayList<ChestShop>> map = table.get(cx, cz);
			if(map == null){
				map = new HashMap<>();

				table.put(cx, cz, map);
			}

			map.computeIfAbsent(data.getWorldName(), (name) -> new ArrayList<>()).add(data);
		});

		table.cellSet().forEach((cell) -> {
			Integer cx = cell.getRowKey(), cz = cell.getColumnKey();
			HashMap<String, ArrayList<ChestShop>> map = cell.getValue();

			if(cx == null || cz == null || map == null) return;

			map.forEach((worldName, list) -> {
				World world = Bukkit.getWorld(worldName);

				if(world == null){
					plugin.getLogger().warning("ChestShop with unknown world \"" + worldName + "\" found!");

					return;
				}

				Chunk chunk = world.getChunkAt(cx, cz);
				if(!chunk.isLoaded()) chunk.load();

				list.forEach((data) -> {

					Block block = chunk.getBlock(data.getX() & 15, data.getY(), data.getZ() & 15);
					if(!CONTAINER_MATERIALS.contains(block.getType())) {
						toBeRemoved.add(data);
					}

				});
			});
		});

		toBeRemoved.forEach((data) -> {

			try {
				chestShopDao.delete(data);
			} catch (SQLException e) {
				e.printStackTrace();
			}

		});

		if(!toBeRemoved.isEmpty()){

			plugin.getLogger().warning("Removed " + toBeRemoved.size() + " invalid ChestShops from cache");

		}

		return input.stream().filter(data -> !toBeRemoved.contains(data)).collect(Collectors.toList());
	}

	private void locationSelector(Where<ChestShop, UUID> where, ChestShop data) throws SQLException{
		where.eq("worldName", data.getWorldName())
				.and()
				.eq("x", data.getX())
				.and()
				.eq("y", data.getY())
				.and()
				.eq("z", data.getZ());
	}

	private void save(List<ChestShop> list){
		list.forEach((data) -> {

			try {
				if(data.getUuid() == null) {
					DeleteBuilder<ChestShop, UUID> deleteBuilder =
							chestShopDao.deleteBuilder();

					locationSelector(deleteBuilder.where(), data);

					deleteBuilder.delete();
				} else {
					UpdateBuilder<ChestShop, UUID> updateBuilder =
							chestShopDao.updateBuilder();

					locationSelector(updateBuilder.where(), data);

					updateBuilder.updateColumnValue("uuid", data.getUuid());

					int rows = updateBuilder.update();
					if(rows == 0){
						chestShopDao.create(data);
					}
				}

			} catch (SQLException ex){
				ex.printStackTrace();
			}

		});
	}

	public void removeChestShop(Location location){
		addChestShop(location, null);
	}

	public void addChestShop(Location location, UUID uuid){
		if(location.getWorld() == null) return;

		updateQueue.add(new ChestShop(null, location.getWorld().getName(), location.getBlockX(), location.getBlockY(), location.getBlockZ(), uuid));
	}

	public boolean hasChestShops(UUID uuid){
		return this.uuidToShopCache.containsKey(uuid);
	}

	public void saveAll(){
		save(updateQueue.stream().toList());
	}

}
