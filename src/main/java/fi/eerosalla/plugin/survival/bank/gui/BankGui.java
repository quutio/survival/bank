package fi.eerosalla.plugin.survival.bank.gui;

import fi.eerosalla.plugin.survival.bank.Bank;
import fi.eerosalla.plugin.survival.bank.gui.views.AccountView;

public class BankGui {

	public static void registerGenerators(Bank plugin, GuiManager manager){
		manager.registerGenerator("AccountView", (player) ->
				new AccountView(manager.getNextWindowId(player), plugin, player.getUniqueId())
		);
	}

}
