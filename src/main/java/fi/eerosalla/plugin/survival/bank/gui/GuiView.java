package fi.eerosalla.plugin.survival.bank.gui;

import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public abstract class GuiView {

	@Getter
	private final int windowId;
	public GuiView(int windowId){
		this.windowId = windowId;
	}

	public abstract GuiClickResponse onClick(int x, boolean shift);

	public abstract ItemStack[] getItems();

	public ItemStack getItem(int x){
		final ItemStack[] items = getItems();

		if(x < 0 || x >= items.length) return new ItemStack(Material.AIR);

		return items[x];
	}

	public abstract String getTitle();

	public String getType(){
		return "generic_9x3";
	}

	@SuppressWarnings("EmptyMethod")
	public void onClose(){}
}
