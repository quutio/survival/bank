package fi.eerosalla.plugin.survival.bank.util;


import java.util.*;

public class HashMapSet<T> {

	private final HashMap<Integer, HashSet<T>> map;
	public HashMapSet(){
		map = new HashMap<>();
	}

	public void add(T obj){
		map.computeIfAbsent(obj.hashCode(), h -> new HashSet<>()).add(obj);
	}

	public boolean contains(T obj){
		HashSet<T> set = map.get(obj.hashCode());

		return set != null && set.contains(obj);
	}

	public T remove(T obj){
		int hashcode = obj.hashCode();

		HashSet<T> set = map.get(hashcode);
		if(set == null) return null;

		Iterator<T> it = set.iterator();
		while(it.hasNext()){
			T obj2 = it.next();
			if(obj2.equals(obj)){
				it.remove();

				if(set.isEmpty()){
					map.remove(hashcode);
				}

				return obj2;
			}
		}

		return null;
	}

	public List<T> values(){
		ArrayList<T> list = new ArrayList<>();

		map.values().forEach(list::addAll);

		return list;
	}

}
