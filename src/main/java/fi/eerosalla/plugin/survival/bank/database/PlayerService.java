package fi.eerosalla.plugin.survival.bank.database;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import fi.eerosalla.plugin.survival.bank.Bank;
import fi.eerosalla.plugin.survival.bank.database.model.BankPlayer;
import fi.eerosalla.plugin.survival.bank.util.MojangUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import static fi.eerosalla.plugin.survival.bank.util.SchedulerUtil.*;

public class PlayerService {

	private Dao<BankPlayer, UUID> bankPlayerDao;

	private final ConcurrentHashMap<UUID, BankPlayer> cache = new ConcurrentHashMap<>();

	public PlayerService(Bank plugin, ConnectionSource connectionSource) {
		try {

			bankPlayerDao = DaoManager.createDao(connectionSource, BankPlayer.class);

			TableUtils.createTableIfNotExists(connectionSource, BankPlayer.class);

		} catch (SQLException ex) {
			ex.printStackTrace();
		}


		plugin.getServer().getScheduler().runTaskTimer(plugin, () -> {

			final List<BankPlayer> updatedObjects = cache.values().stream()
					.filter(BankPlayer::save)
					.map(BankPlayer::clone)
					.collect(Collectors.toList());

			if(updatedObjects.isEmpty()) return;

			final long now = System.currentTimeMillis();
			updatedObjects.stream()
							.filter(data ->
									!(data.getKeepUntil() <= now
											|| Bukkit.getPlayer(data.getUuid()) != null
											|| plugin.chestShopService.hasChestShops(data.getUuid())
									))
					.map(BankPlayer::getUuid)
					.forEach(cache::remove);

			async(() -> updatedObjects.forEach((data) -> {
				try {
					bankPlayerDao.update(data);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}));

		}, 0L, 200L);

	}

	public QueryBuilder<BankPlayer, UUID> query(){
		return bankPlayerDao.queryBuilder();
	}

	public void saveAll() {
		cache.forEach((uuid, data) -> {
			try {
				bankPlayerDao.update(data);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
	}

	public boolean loadPlayerData(Player player) {
		BankPlayer data;

		try {
			data = bankPlayerDao.queryForId(player.getUniqueId());

			if (data == null) {
				data = new BankPlayer(player.getUniqueId(), player.getName().toLowerCase(), 0L);

				bankPlayerDao.create(data);
			} else {
				if (!player.getName().equals(data.getName())) {
					data.setName(player.getName());

					bankPlayerDao.update(data);
				}
			}

			cache.put(player.getUniqueId(), data);

		} catch (SQLException e) {
			e.printStackTrace();

			return false;
		}

		return true;
	}

	public void loadPlayerData(UUID uuid){
		if(cache.containsKey(uuid)) return;
		BankPlayer data;

		try {

			data = bankPlayerDao.queryForId(uuid);

			if(data != null){
				cache.put(uuid, data);
			}

		} catch (SQLException ex){
			ex.printStackTrace();
		}
	}

	public void scheduleSave(UUID uuid) {
		cache.get(uuid).markUnsaved();
	}

	public BankPlayer getPlayerFromCache(UUID uuid) {
		return cache.get(uuid);
	}

	public Future<BankPlayer> getPlayer(String inputName) {
		String name = inputName.toLowerCase();

		CompletableFuture<BankPlayer> future = new CompletableFuture<>();

		Player onlinePlayer = Bukkit.getPlayerExact(name);
		if (onlinePlayer != null) {

			BankPlayer data = cache.get(onlinePlayer.getUniqueId());
			data.setKeep(10);

			future.complete(data);
		} else {

			try {
				List<BankPlayer> list = bankPlayerDao.queryForEq("name", name);

				if (list.isEmpty()) {
					future.complete(null);
				} else if (list.size() == 1) {
					BankPlayer data = list.get(0);
					data.setKeep(10);

					future.complete(data);
				} else { //Fix duplicates
					UUID correctUuid = MojangUtil.fetchUuidForName(name);

					for(BankPlayer data : list){
						if(data.getUuid().equals(correctUuid)){
							future.complete(data);
						} else {
							data.setName(null);
						}
					}

					UpdateBuilder<BankPlayer, UUID> updateBuilder = bankPlayerDao.updateBuilder();
					updateBuilder.where()
							.eq("name", name)
							.and()
							.not()
							.eq("uuid", correctUuid);

					updateBuilder.updateColumnValue("name", null);

					updateBuilder.update();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (!future.isDone()) {
				future.complete(null);
			}
		}

		return future;
	}
}
