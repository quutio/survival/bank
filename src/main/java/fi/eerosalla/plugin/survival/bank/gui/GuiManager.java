package fi.eerosalla.plugin.survival.bank.gui;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.reflect.StructureModifier;
import fi.eerosalla.plugin.survival.bank.Bank;
import fi.eerosalla.plugin.survival.bank.util.PacketUtil;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_18_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.UUID;
import java.util.function.Function;

import static fi.eerosalla.plugin.survival.bank.util.SchedulerUtil.sync;

public class GuiManager {

	private final HashMap<UUID, GuiView> openViews;
	private final HashMap<String, Function<Player, GuiView>> generators;

	public int getNextWindowId(Player player){
		return ((CraftPlayer) player).getHandle().nextContainerCounter();
	}


	public GuiManager(Bank plugin){

		this.openViews = new HashMap<>();
		this.generators = new HashMap<>();

		final ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();
		protocolManager.addPacketListener(new PacketAdapter(plugin, PacketType.Play.Client.WINDOW_CLICK, PacketType.Play.Client.CLOSE_WINDOW) {
			@Override
			public void onPacketReceiving(PacketEvent event) {

				sync(() -> {

					Player player = event.getPlayer();

					GuiView view = openViews.get(player.getUniqueId());
					if(view == null){
						return;
					}

					PacketContainer packet = event.getPacket();

					if(packet.getType().equals(PacketType.Play.Client.WINDOW_CLICK)){ //Window click

						StructureModifier<Integer> integers = packet.getIntegers();
						int windowId = integers.read(0),
								clickedSlotId = integers.read(2);


						int viewSize = view.getItems().length;

						InventoryClickType clickType = packet.getEnumModifier(InventoryClickType.class, 4).read(0);

						//noinspection unchecked
						Int2ObjectMap<net.minecraft.world.item.ItemStack> map = (Int2ObjectMap<net.minecraft.world.item.ItemStack>) packet.getModifier().read(6);
						map.forEach((slot, item) -> {
							if(slot < viewSize){

								PacketContainer resetItemPacket = PacketUtil.getSetSlotPacket(windowId, slot, view.getItem(slot));
								try {
									protocolManager.sendServerPacket(player, resetItemPacket);
								} catch (InvocationTargetException e) {
									e.printStackTrace();
								}

							} else {

								int inventorySlot;
								if(slot < viewSize + 27){
									inventorySlot = slot - viewSize + 9;
								} else {
									inventorySlot = slot - viewSize - 27;
								}

								if(inventorySlot >= 0 && inventorySlot <= 35) {

									PacketContainer resetItemPacket = PacketUtil.getSetSlotPacket(windowId, slot, player.getInventory().getItem(inventorySlot));
									try {
										protocolManager.sendServerPacket(player, resetItemPacket);
									} catch (InvocationTargetException e) {
										e.printStackTrace();
									}

								}
							}
						});

						PacketContainer clearHeldItemPacket = PacketUtil.getSetSlotPacket(-1, -1, new ItemStack(Material.AIR));
						try {
							protocolManager.sendServerPacket(player, clearHeldItemPacket);
						} catch (InvocationTargetException e) {
							e.printStackTrace();
						}

						GuiClickResponse response = view.onClick(clickedSlotId, clickType.equals(InventoryClickType.QUICK_MOVE));
						switch (response.getType()) {
							case CLOSE_WINDOW -> {
								try {
									protocolManager.sendServerPacket(player,
											PacketUtil.getWindowClosePacket(view.getWindowId()));
								} catch (InvocationTargetException ex) {
									ex.printStackTrace();
								}

								view.onClose();
							}
							case UPDATE_SLOTS -> {
								GuiClickResponse.UpdateSlots updateSlots = (GuiClickResponse.UpdateSlots) response;

								for (int slot : updateSlots.getSlots()) {
									try {
										protocolManager.sendServerPacket(player,
												PacketUtil.getSetSlotPacket(view.getWindowId(), slot,
														view.getItem(slot)));
									} catch (InvocationTargetException ex) {
										ex.printStackTrace();
									}
								}
							}
							case CHANGE_VIEW -> {
								GuiClickResponse.ChangeView changeView = (GuiClickResponse.ChangeView) response;

								openView(player, changeView.getId());
							}
						}

					} else { //Window close

						view.onClose();
						openViews.remove(player.getUniqueId());

					}

				});

			}
		});


		protocolManager.addPacketListener(new PacketAdapter(plugin, PacketType.Play.Server.OPEN_WINDOW){
			@Override
			public void onPacketSending(PacketEvent event) {

				sync(() -> {

					Player player = event.getPlayer();

					GuiView view = openViews.get(player.getUniqueId());
					if(view != null && view.getWindowId() != event.getPacket().getIntegers().read(0)){

						view.onClose();
						openViews.remove(player.getUniqueId());

					}

				});

			}
		});
	}

	public void registerGenerator(String id, Function<Player, GuiView> generator){
		generators.put(id, generator);
	}

	public void openView(Player player, String id){
		Function<Player, GuiView> generator = generators.get(id);

		if(generator == null) return;

		GuiView view = generator.apply(player);

		GuiView oldView = openViews.put(player.getUniqueId(), view);
		if(oldView != null){

			oldView.onClose();

		}

		PacketContainer openWindowPacket = PacketUtil.getWindowOpenPacket(view.getWindowId(), view.getType(), view.getTitle());
		PacketContainer windowItemsPacket = PacketUtil.getWindowItemsPacket(view.getWindowId(), view.getItems());


		ProtocolManager protocolManager = ProtocolLibrary.getProtocolManager();
		try {

			protocolManager.sendServerPacket(player, openWindowPacket);
			protocolManager.sendServerPacket(player, windowItemsPacket);

		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

	}

	public void handleQuit(Player player){
		GuiView view = openViews.get(player.getUniqueId());

		if(view != null){

			view.onClose();
			openViews.remove(player.getUniqueId());

		}
	}

}
