# Bank

Bank is an economy plugin used in Quutio's Survival gamemode.

It works similarly to Gringotts and GoldIsMoney, but it uses virtual vaults instead of containers.
